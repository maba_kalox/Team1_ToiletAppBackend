FROM nginx:alpine
COPY /nginx/conf.d/ /etc/nginx/conf.d/
RUN apk add openssl
RUN openssl req -x509 -nodes -days 365 -subj "/C=LV/ST=RIGA/O=toilet_app, Inc./CN=toilet_app.io" -addext "subjectAltName=DNS:toilet_app.io" -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt