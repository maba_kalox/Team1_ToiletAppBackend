package org.toiletapp.helpers;

public class Response_Pojo<ResultType> {
    private final ResultType result;

    public Response_Pojo(ResultType result) {
        this.result = result;
    }

    public ResultType getResult() {
        return result;
    }
}
