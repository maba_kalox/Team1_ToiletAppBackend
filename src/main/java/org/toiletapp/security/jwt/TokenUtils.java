package org.toiletapp.security.jwt;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;


import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.auth.oauth2.GooglePublicKeysManager;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import io.smallrye.jwt.build.Jwt;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.toiletapp.security.RolesEnum;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.stream.Collectors;

@RequestScoped
public class TokenUtils {
    @ConfigProperty(name = "mp.jwt.verify.issuer")
    String jwt_issuer;

    @ConfigProperty(name = "org.toiletapp.security.google_client_id")
    String google_client_id;

    private static final Logger LOG = Logger.getLogger(TokenUtils.class);

    GooglePublicKeysManager googlePublicKeysManager = new GooglePublicKeysManager(new NetHttpTransport(), new GsonFactory());

    /**
     * Create Signed JWT Token
     *
     * @param member_id member_db_id
     * @param roles     array of user rolles
     * @return Signed JWT token string
     */
    public String generate_token(String member_id, EnumSet<RolesEnum> roles) {
        return Jwt
                .issuer(jwt_issuer)
                .groups(new HashSet<>(roles.stream().map(RolesEnum::toString).collect(Collectors.toList())))
                .claim("member_id", member_id)
                .sign();
    }

    public static int currentTimeInSecs() {
        long currentTimeMS = System.currentTimeMillis();
        return (int) (currentTimeMS / 1000);
    }

    public Payload verify_google_token_and_get_payload(String google_token) {
        NotAuthorizedException validation_error = new NotAuthorizedException(Response.status(401)
                .entity("invalid_token").build());
        GoogleIdTokenVerifier google_verifier = new GoogleIdTokenVerifier.Builder(googlePublicKeysManager).setAudience(Collections.singletonList(google_client_id)).build();
        try {
            GoogleIdToken idToken = google_verifier.verify(google_token);
            if (idToken != null) {
                return idToken.getPayload();
            } else {
                throw validation_error;
            }
        } catch (GeneralSecurityException e) {
            throw validation_error;
        } catch (IOException | IllegalArgumentException e) {
            throw new BadRequestException(Response.status(400)
                    .entity("invalid_token_format").build());
        }
    }
}
