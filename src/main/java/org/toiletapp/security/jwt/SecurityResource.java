package org.toiletapp.security.jwt;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import org.toiletapp.entities.User.UserModel;
import org.toiletapp.entities.User.UserService;
import org.toiletapp.security.RolesEnum;
import org.toiletapp.security.jwt.Pojo.GoogleTokenSwapPojo;

import java.util.UUID;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
@PermitAll
public class SecurityResource {
    @Inject
    UserService userService;

    @Inject
    TokenUtils tokenUtils;

    @POST
    @Path("/google_token_swap")
    @Consumes(MediaType.APPLICATION_JSON)
    public Map<String, String> google_token_swap(GoogleTokenSwapPojo info) {
        Map<String, String> response = new HashMap<>();
        response.put("token_type", "Bearer");

        GoogleIdToken.Payload G_token_payload = tokenUtils.verify_google_token_and_get_payload(info.getGoogle_token());
        UserModel user = userService.get_by_google_id(G_token_payload.getSubject());
        if (user == null) {
            user = userService.create(G_token_payload.getSubject());
        } else if (!user.getIs_active()) {
            throw new NotAuthorizedException(Response.status(401).entity("your account was deactivated, ask support.").build());
        }
        response.put("token", tokenUtils.generate_token(user.getUser_id().toString(), EnumSet.of(RolesEnum.USER)));
        return response;
    }

    @GET
    @Path("/get_foolish_token")
    public Map<String, String> get_foolish_token() {
        Map<String, String> response = new HashMap<>();
        response.put("token_type", "Bearer");
        String uuidAsString = UUID.randomUUID().toString();
        UserModel user = userService.create(String.format("it_is_not_google_id-%s", uuidAsString));
        response.put("token", tokenUtils.generate_token(user.getUser_id().toString(), EnumSet.of(RolesEnum.USER)));
        return response;
    }
}
