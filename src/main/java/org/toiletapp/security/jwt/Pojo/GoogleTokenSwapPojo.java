package org.toiletapp.security.jwt.Pojo;

public class GoogleTokenSwapPojo {
    private String google_token;

    public String getGoogle_token() {
        return google_token;
    }

    public void setGoogle_token(String google_token) {
        this.google_token = google_token;
    }
}
