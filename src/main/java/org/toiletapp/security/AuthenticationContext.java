package org.toiletapp.security;

import org.toiletapp.entities.User.UserModel;

public interface AuthenticationContext {
    UserModel getCurrentUser();
}
