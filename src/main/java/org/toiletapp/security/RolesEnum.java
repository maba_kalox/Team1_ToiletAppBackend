package org.toiletapp.security;

public enum RolesEnum {
    USER(Constants.USER_STR);

    RolesEnum(String roleString) {}

    public static class Constants {
        public static final String USER_STR = "USER";
    }
}
