package org.toiletapp.security;

import org.toiletapp.entities.User.UserModel;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;

@RequestScoped
public class AuthenticationContextImpl implements AuthenticationContext {
    private UserModel user;

    @Override
    public UserModel getCurrentUser() {
        return user;
    }

    public void setCurrentUser(UserModel user) {
        if (user == null) {
            throw new NotAuthorizedException(Response.status(401).entity("your_account_doesnt_exist").build());
        } else if (!user.getIs_active()) {
            throw new NotAuthorizedException(Response.status(401).entity("your_account_is_not_activated").build());
        } else {
            this.user = user;
        }
    }
}
