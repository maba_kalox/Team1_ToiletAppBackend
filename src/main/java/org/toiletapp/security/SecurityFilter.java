package org.toiletapp.security;

import io.quarkus.security.Authenticated;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.toiletapp.entities.User.UserModel;
import org.toiletapp.entities.User.UserService;

import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@PreMatching
public class SecurityFilter implements ContainerRequestFilter {

    @Inject
    AuthenticationContextImpl authCtx; // Injecting the implementation,
    // not the interface!!!

    @Inject
    JsonWebToken jwt;

    @Inject
    UserService userService;

    @Context
    SecurityContext securityCtx;

    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (jwt != null && jwt.getClaim("member_id") != null) {
            UserModel user = userService.get_by_id(Long.parseLong(jwt.getClaim("member_id")));
            authCtx.setCurrentUser(user);
        }
    }
}