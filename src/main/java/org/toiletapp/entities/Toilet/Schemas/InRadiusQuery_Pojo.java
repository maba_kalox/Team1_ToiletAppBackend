package org.toiletapp.entities.Toilet.Schemas;

import org.locationtech.jts.geom.Point;

public class InRadiusQuery_Pojo {
    private Point center_geo_point;

    private double radius_m;

    public Point getCenter_geo_point() {
        return center_geo_point;
    }

    public void setCenter_geo_point(Point center_geo_point) {
        this.center_geo_point = center_geo_point;
    }

    public double getRadius_m() {
        return radius_m;
    }

    public void setRadius_m(double radius_m) {
        this.radius_m = radius_m;
    }
}
