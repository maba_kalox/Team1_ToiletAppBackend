package org.toiletapp.entities.Toilet.Schemas;

import org.locationtech.jts.geom.Point;

public class NewToiletPojo {

    private Point geo_point;

    private boolean is_free;

    public Point getGeo_point() {
        return geo_point;
    }

    public void setGeo_point(Point geo_point) {
        this.geo_point = geo_point;
    }

    public boolean getIs_free() {
        return is_free;
    }

    public void setIs_free(boolean is_free) {
        this.is_free = is_free;
    }
}
