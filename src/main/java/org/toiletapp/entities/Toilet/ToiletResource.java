package org.toiletapp.entities.Toilet;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import org.toiletapp.entities.Toilet.Schemas.InRadiusQuery_Pojo;
import org.toiletapp.entities.Toilet.Schemas.NewToiletPojo;
import org.toiletapp.helpers.Response_Pojo;
import org.toiletapp.security.AuthenticationContext;
import org.toiletapp.security.RolesEnum;

@Produces(MediaType.APPLICATION_JSON)
@Path("/toilets")
@RolesAllowed({RolesEnum.Constants.USER_STR})
public class ToiletResource {

    @Inject
    ToiletService toiletService;

    @Inject
    AuthenticationContext authenticationContext;

    @POST
    @Path("/in_radius")
    @Consumes(MediaType.APPLICATION_JSON)
    public List<ToiletModel> in_radius(InRadiusQuery_Pojo data) {
        return toiletService.in_radius(data.getCenter_geo_point(), data.getRadius_m());
    }

    @POST
    @Path("/count_in_radius")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response_Pojo<Long> count_in_radius(InRadiusQuery_Pojo data) {
        return new Response_Pojo<>(toiletService.count_in_radius(data.getCenter_geo_point(), data.getRadius_m()));
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public ToiletModel create(NewToiletPojo newToiletPojo) {
        return toiletService.create(newToiletPojo, authenticationContext.getCurrentUser());
    }

    @DELETE
    @Path("/delete")
    @Transactional
    public Response_Pojo<Boolean> delete(@QueryParam("toilet_id") long toilet_id) {
        if (!toiletService.exists(toilet_id)) {
            throw new BadRequestException(Response.status(400)
                    .entity("toilet_doesnt_exist").build());
        }
        toiletService.delete(toiletService.get_reference_by_id(toilet_id));

        return new Response_Pojo<>(true);
    }
}