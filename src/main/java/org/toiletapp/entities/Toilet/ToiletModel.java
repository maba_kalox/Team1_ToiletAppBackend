package org.toiletapp.entities.Toilet;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenerationTime;
import org.locationtech.jts.geom.Point;
import org.toiletapp.entities.Review.ReviewModel;
import org.toiletapp.entities.User.UserModel;

@Entity
@Table(name = "Toilets")
public class ToiletModel {
    @Id
    @SequenceGenerator(name = "Toilets_toilet_id_seq",
            sequenceName = "Toilets_toilet_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "Toilets_toilet_id_seq")
    @Column(updatable = false)
    private Long toilet_id;

    @GeneratedValue
    @Column(updatable=false, insertable=false)
    private Timestamp create_timestamp;

    private Timestamp edit_timestamp;

    private Boolean is_free;

    @Column(columnDefinition = "geography(POINT)")
    private Point geo_point;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel user;

    @JsonIgnore
    @OneToMany(mappedBy = "toilet")
    private List<ReviewModel> reviews;

    public Long getToilet_id() {
        return toilet_id;
    }

    private void setToilet_id(Long toilet_id) {
        this.toilet_id = toilet_id;
    }

    public Timestamp getCreate_timestamp() {
        return create_timestamp;
    }

    private void setCreate_timestamp(Timestamp create_timestamp) {
        this.create_timestamp = create_timestamp;
    }

    public Timestamp getEdit_timestamp() {
        return edit_timestamp;
    }

    private void setEdit_timestamp(Timestamp edit_timestamp) {
        this.edit_timestamp = edit_timestamp;
    }

    @PreUpdate
    protected void on_update() {
        setEdit_timestamp(Timestamp.from(Instant.now()));
    }

    public Boolean getIs_free() {
        return is_free;
    }

    public void setIs_free(Boolean is_free) {
        this.is_free = is_free;
    }

    public Point getGeo_point() {
        return geo_point;
    }

    public void setGeo_point(Point geo_point) {
        this.geo_point = geo_point;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public List<ReviewModel> getReviews() {
        return reviews;
    }
}
