package org.toiletapp.entities.Toilet;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.locationtech.jts.geom.Point;
import org.toiletapp.entities.Toilet.Schemas.NewToiletPojo;
import org.toiletapp.entities.User.UserModel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
public class ToiletService {
    @Inject
    EntityManager em;

    @ConfigProperty(name = "org.toiletapp.toilet.overlap_meters")
    double toilet_tolerance_meters;

    public List<ToiletModel> in_radius(Point center, double radius_m) {
        Query query = em.createQuery(
                "SELECT t FROM ToiletModel t WHERE st_dwithin(t.geo_point, :center, :radius_m) = TRUE");
        return query.setParameter("center", center).setParameter("radius_m", radius_m).getResultList();
    }

    public long count_in_radius(Point center, double radius_m) {
        Query query = em.createQuery("SELECT COUNT(*) FROM ToiletModel t WHERE st_dwithin(t.geo_point, :center, :radius_m) = TRUE");
        return (Long) query.setParameter("center", center).setParameter("radius_m", radius_m).getSingleResult();
    }

    public ToiletModel get_reference_by_id(long toilet_id) {
        return em.getReference(ToiletModel.class, toilet_id);
    }

    public boolean exists(long toilet_id) {
        return (long) em.createQuery("select count (1) from ToiletModel where toilet_id=:toilet_id").setParameter("toilet_id", toilet_id).getSingleResult() == 1;
    }

    @Transactional
    public ToiletModel create(NewToiletPojo newToiletPojo, UserModel user) {
        if (count_in_radius(newToiletPojo.getGeo_point(), toilet_tolerance_meters) > 0) {
            throw new BadRequestException(Response.status(400)
                    .entity("possible_toilets_overlap").build());
        }
        ToiletModel new_toilet = new ToiletModel();
        new_toilet.setGeo_point(newToiletPojo.getGeo_point());
        new_toilet.setIs_free(newToiletPojo.getIs_free());
        new_toilet.setUser(user);
        em.persist(new_toilet);
        return new_toilet;
    }

    @Transactional
    public void delete(ToiletModel review) {
        em.remove(review);
    }
}
