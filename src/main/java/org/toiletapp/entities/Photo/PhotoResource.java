package org.toiletapp.entities.Photo;

import org.apache.tika.Tika;
import org.jboss.logging.Logger;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.toiletapp.entities.Review.ReviewModel;
import org.toiletapp.entities.Review.ReviewService;
import org.toiletapp.helpers.MultipartBody;
import org.toiletapp.helpers.Response_Pojo;
import org.toiletapp.security.RolesEnum;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedInputStream;
import java.io.IOException;

@Produces(MediaType.APPLICATION_JSON)
@Path("/photos")
@RolesAllowed({RolesEnum.Constants.USER_STR})
public class PhotoResource {
    @Inject
    PhotoService photoService;

    @Inject
    ReviewService reviewService;

    private static final Logger LOG = Logger.getLogger(PhotoResource.class);

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public PhotoModel upload(@MultipartForm MultipartBody file, @QueryParam("review_id") long review_id) throws IOException {
        if (!reviewService.exists(review_id)) {
            throw new BadRequestException(Response.status(400)
                    .entity("review_doesnt_exist").build());
        }
        ReviewModel review = reviewService.get_reference_by_id(review_id);
        file.file = new BufferedInputStream(file.file);
        String detectedFileType;
        try {
            detectedFileType = (new Tika()).detect(file.file);
        } catch (IOException e) {
            throw new BadRequestException(Response.status(500)
                    .entity("file_read_fail").build());
        }
        if (!detectedFileType.contains("image")) {
            throw new BadRequestException(Response.status(400)
                    .entity("file_is_not_image").build());
        }
        return photoService.create(
                file.file, detectedFileType.substring(detectedFileType.indexOf('/') + 1), review);
    }

    @DELETE
    @Path("/delete")
    @Transactional
    public Response_Pojo<Boolean> delete(@QueryParam("photo_id") long photo_id) {
        if (!photoService.exists(photo_id)) {
            throw new BadRequestException(Response.status(400)
                    .entity("phoyo_doesnt_exist").build());
        }
        photoService.delete(photoService.get_reference_by_id(photo_id));

        return new Response_Pojo<>(true);
    }
}
