package org.toiletapp.entities.Photo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.toiletapp.entities.Review.ReviewModel;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
@Table(name = "Photos")
public class PhotoModel {
    @Id
    @SequenceGenerator(name = "Photos_photo_id_seq",
            sequenceName = "Photos_photo_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "Photos_photo_id_seq")
    @Column(updatable = false)
    private Long photo_id;

    private String saved_file_path;

    @GeneratedValue
    @Column(updatable=false, insertable=false)
    private Timestamp create_timestamp;

    private Timestamp edit_timestamp;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "review_id", nullable = false)
    private ReviewModel review;

    public Long getPhoto_id() {
        return photo_id;
    }

    private void setPhoto_id(Long photo_id) {
        this.photo_id = photo_id;
    }

    public String getSaved_file_path() {
        return saved_file_path;
    }

    public void setSaved_file_path(String saved_file_path) {
        this.saved_file_path = saved_file_path;
    }

    public Timestamp getCreate_timestamp() {
        return create_timestamp;
    }

    private void setCreate_timestamp(Timestamp create_timestamp) {
        this.create_timestamp = create_timestamp;
    }

    public Timestamp getEdit_timestamp() {
        return edit_timestamp;
    }

    private void setEdit_timestamp(Timestamp edit_timestamp) {
        this.edit_timestamp = edit_timestamp;
    }

    @PreUpdate
    protected void on_update() {
        setEdit_timestamp(Timestamp.from(Instant.now()));
    }

    public ReviewModel getReview() {
        return review;
    }

    public void setReview(ReviewModel review) {
        this.review = review;
    }
}
