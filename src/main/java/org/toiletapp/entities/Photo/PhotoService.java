package org.toiletapp.entities.Photo;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.toiletapp.entities.Review.ReviewModel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.*;

@ApplicationScoped
public class PhotoService {
    @Inject
    EntityManager em;

    @ConfigProperty(name = "org.toiletapp.photo.save_path")
    String photo_save_path;

    public PhotoModel get_reference_by_id(long photo_id) {
        return em.getReference(PhotoModel.class, photo_id);
    }

    public boolean exists(long photo_id) {
        return (long) em.createQuery("select count (1) from PhotoModel where photo_id=:photo_id").setParameter("photo_id", photo_id).getSingleResult() == 1;
    }

    @Transactional
    public PhotoModel create(InputStream file_stream, String file_extension, ReviewModel review) throws IOException {
        PhotoModel new_photo = new PhotoModel();
        new_photo.setReview(review);
        em.persist(new_photo);
        String file_name = String.format("%d.%s", new_photo.getPhoto_id(), file_extension);
        new_photo.setSaved_file_path(String.format("%s/%s", photo_save_path, file_name));
        try {
            FileOutputStream out = new FileOutputStream(new_photo.getSaved_file_path());
            out.write(file_stream.readAllBytes());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        em.persist(new_photo);
        return new_photo;
    }

    @Transactional
    public void delete(PhotoModel photo) {
        File photo_file = new File(photo.getSaved_file_path());
        if (photo_file.exists()) {
            photo_file.delete();
        }
        em.remove(photo);
    }
}
