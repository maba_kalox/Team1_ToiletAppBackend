package org.toiletapp.entities.User;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.jwt.JsonWebToken;
import org.toiletapp.security.AuthenticationContext;
import org.toiletapp.security.RolesEnum;
import org.toiletapp.security.jwt.TokenUtils;

@Path("/users")
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({RolesEnum.Constants.USER_STR})
public class UserResource {

    @Inject
    UserService userService;

    @Inject
    TokenUtils tokenUtils;

    @Inject
    AuthenticationContext authenticationContext;

    @GET
    @Path("/me")
    public UserModel me() {
        return authenticationContext.getCurrentUser();
    }
}