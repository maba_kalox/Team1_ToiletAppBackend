package org.toiletapp.entities.User;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ColumnDefault;
import org.toiletapp.entities.Review.ReviewModel;
import org.toiletapp.entities.Toilet.ToiletModel;

import java.util.List;

@Entity
@Table(name = "Users")
public class UserModel {
    @Id
    @SequenceGenerator(name = "Users_user_id_seq",
            sequenceName = "Users_user_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "Users_user_id_seq")
    @Column(updatable = false)
    private Long user_id;
    private String google_id;

    @GeneratedValue
    private boolean is_active;

    public Long getUser_id() {
        return user_id;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<ToiletModel> toilets;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<ReviewModel> reviews;

    private void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getGoogle_id() {
        return google_id;
    }

    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    public List<ToiletModel> getToilets() {
        return toilets;
    }

    public List<ReviewModel> getReviews() { return reviews; }

    public boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }
}
