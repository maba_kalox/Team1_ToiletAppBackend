package org.toiletapp.entities.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Hashtable;

@ApplicationScoped
public class UserService {
    @Inject
    EntityManager em;

    @Transactional
    public void createUser(String user_google_id) {
        UserModel user = new UserModel();
        user.setGoogle_id(user_google_id);
        em.persist(user);
    }

    public UserModel get_by_google_id(String user_google_id) {
        Query query = em.createQuery(
                "SELECT u " +
                        "FROM UserModel u WHERE u.google_id = :target_google_user_id");
        try {
            return (UserModel) query.setParameter("target_google_user_id", user_google_id).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public UserModel get_by_id(long user_id) {
        return em.find(UserModel.class, user_id);
    }

    @Transactional
    public UserModel create(String google_id) {
        UserModel new_user = new UserModel();
        new_user.setGoogle_id(google_id);
        new_user.setIs_active(true);
        em.persist(new_user);
        return new_user;
    }
}
