package org.toiletapp.entities.Review.Schemas;

public class EditReview_Pojo {
    String comment;

    short rating;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public short getRating() {
        return rating;
    }

    public void setRating(short rating) {
        this.rating = rating;
    }
}
