package org.toiletapp.entities.Review.Schemas;

public class CreateReview_Pojo extends EditReview_Pojo {
    long toilet_id;

    public long getToilet_id() {
        return toilet_id;
    }

    public void setToilet_id(long toilet_id) {
        this.toilet_id = toilet_id;
    }
}
