package org.toiletapp.entities.Review;

import org.toiletapp.entities.Review.Schemas.CreateReview_Pojo;
import org.toiletapp.entities.Review.Schemas.EditReview_Pojo;
import org.toiletapp.helpers.Response_Pojo;
import org.toiletapp.security.AuthenticationContext;
import org.toiletapp.security.RolesEnum;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Produces(MediaType.APPLICATION_JSON)
@Path("/reviews")
@RolesAllowed({RolesEnum.Constants.USER_STR})
public class ReviewResource {

    @Inject
    ReviewService reviewService;

    @Inject
    AuthenticationContext authenticationContext;

    @GET
    @Path("/get_by_toilet_id")
    public List<ReviewModel> get_by_toilet_id(@QueryParam("toilet_id") long toilet_id) {
        return reviewService.get_by_toilet_id(toilet_id);
    }

    @GET
    @Path("/get_by_id")
    public ReviewModel get_by_id(@QueryParam("review_id") long review_id) {
        ReviewModel review = reviewService.get_by_id(review_id);
        if (review == null) {
            throw new BadRequestException(Response.status(400)
                    .entity("review_doesnt_exist").build());
        }
        return reviewService.get_by_id(review_id);
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public ReviewModel create(CreateReview_Pojo createReview_pojo) {
        ReviewModel existing_review = reviewService.get_by_toilet_id_and_user_id(
                createReview_pojo.getToilet_id(), authenticationContext.getCurrentUser().getUser_id());
        if (existing_review != null) {
            throw new BadRequestException(Response.status(400)
                    .entity("review_from_you_on_that_toilet_exists").build());
        }
        return reviewService.create(createReview_pojo, authenticationContext.getCurrentUser());
    }

    @POST
    @Path("/edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public ReviewModel edit(EditReview_Pojo editReview_pojo, @QueryParam("review_id") long review_id) {
        if (!reviewService.exists(review_id)) {
            throw new BadRequestException(Response.status(400)
                    .entity("review_doesnt_exist").build());
        }
        return reviewService.edit(reviewService.get_reference_by_id(review_id), editReview_pojo);
    }

    @DELETE
    @Path("/delete")
    @Transactional
    public Response_Pojo<Boolean> delete(@QueryParam("review_id") long review_id) {
        if (!reviewService.exists(review_id)) {
            throw new BadRequestException(Response.status(400)
                    .entity("review_doesnt_exist").build());
        }
        reviewService.delete(reviewService.get_reference_by_id(review_id));

        return new Response_Pojo<>(true);
    }
}
