package org.toiletapp.entities.Review;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.toiletapp.entities.Photo.PhotoModel;
import org.toiletapp.entities.Toilet.ToiletModel;
import org.toiletapp.entities.User.UserModel;

import javax.persistence.*;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "Reviews")
public class ReviewModel {
    @Id
    @SequenceGenerator(name = "Reviews_review_id_seq",
            sequenceName = "Reviews_review_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "Reviews_review_id_seq")
    @Column(updatable = false)
    private Long review_id;

    private String comment;

    private short rating;

    @GeneratedValue
    @Column(updatable=false, insertable=false)
    private Timestamp create_timestamp;

    private Timestamp edit_timestamp;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "toilet_id", nullable = false)
    private ToiletModel toilet;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel user;

//    @JsonIgnore
    @OneToMany(mappedBy = "review")
    private List<PhotoModel> photos;

    @Column(updatable=false, insertable=false)
    private long toilet_id;

    @Column(updatable=false, insertable=false)
    private long user_id;

    public Long getReview_id() {
        return review_id;
    }

    private void setReview_id(Long review_id) {
        this.review_id = review_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public short getRating() {
        return rating;
    }

    public void setRating(short rating) {
        if (rating < 0 || rating > 5) {
            throw new BadRequestException(
                    Response.status(400).entity("rating_out_of_range").build()
            );
        }
        this.rating = rating;
    }

    public Timestamp getCreate_timestamp() {
        return create_timestamp;
    }

    private void setCreate_timestamp(Timestamp create_timestamp) {
        this.create_timestamp = create_timestamp;
    }

    public Timestamp getEdit_timestamp() {
        return edit_timestamp;
    }

    private void setEdit_timestamp(Timestamp edit_timestamp) {
        this.edit_timestamp = edit_timestamp;
    }

    @PreUpdate
    protected void on_update() {
        setEdit_timestamp(Timestamp.from(Instant.now()));
    }

    public ToiletModel getToilet() {
        return toilet;
    }

    public void setToilet(ToiletModel toilet) {
        this.toilet = toilet;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public long getToilet_id() {
        return toilet_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public List<PhotoModel> getPhotos() {
        return photos;
    }

    public void setPhotos(List<PhotoModel> photos) {
        this.photos = photos;
    }
}
