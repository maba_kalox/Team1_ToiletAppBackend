package org.toiletapp.entities.Review;

import org.toiletapp.entities.Review.Schemas.CreateReview_Pojo;
import org.toiletapp.entities.Review.Schemas.EditReview_Pojo;
import org.toiletapp.entities.Toilet.ToiletService;
import org.toiletapp.entities.User.UserModel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
public class ReviewService {
    @Inject
    EntityManager em;

    @Inject
    ToiletService toiletService;

    public List<ReviewModel> get_by_toilet_id(long toilet_id) {
        Query query = em.createQuery(
                "SELECT r FROM ReviewModel r WHERE r.toilet_id = :toilet_id");
        return query.setParameter("toilet_id", toilet_id).getResultList();
    }

    public ReviewModel get_by_toilet_id_and_user_id(long toilet_id, long user_id) {
        Query query = em.createQuery(
                "SELECT r FROM ReviewModel r WHERE r.toilet_id = :toilet_id AND r.user_id = :user_id");
        try {
            return (ReviewModel) query.setParameter("toilet_id", toilet_id).setParameter("user_id", user_id).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public boolean exists(long review_id) {
        return (long) em.createQuery("select count (1) from ReviewModel where review_id=:review_id").setParameter("review_id", review_id).getSingleResult() == 1;
    }

    public ReviewModel get_reference_by_id(long review_id) {
        return em.getReference(ReviewModel.class, review_id);
    }

    public ReviewModel get_by_id(long review_id) {
        return em.find(ReviewModel.class, review_id);
    }

    @Transactional
    public ReviewModel edit(ReviewModel target_review, EditReview_Pojo editReview_pojo) {
        target_review.setRating(editReview_pojo.getRating());
        target_review.setComment(editReview_pojo.getComment());
        em.persist(target_review);
        return target_review;
    }

    @Transactional
    public ReviewModel create(CreateReview_Pojo createReview_pojo, UserModel user) {
        if (!toiletService.exists(createReview_pojo.getToilet_id())) {
            throw new BadRequestException(Response.status(400)
                    .entity("toilet_doesnt_exist").build());
        }

        ReviewModel new_review = new ReviewModel();
        new_review.setComment(createReview_pojo.getComment());
        new_review.setRating(createReview_pojo.getRating());
        new_review.setToilet(toiletService.get_reference_by_id(createReview_pojo.getToilet_id()));
        new_review.setUser(user);
        em.persist(new_review);
        return new_review;
    }

    @Transactional
    public void delete(ReviewModel review) {
        em.remove(review);
    }
}
