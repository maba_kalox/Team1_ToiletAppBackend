CREATE TABLE Photos
(
    photo_id        BIGSERIAL PRIMARY KEY,
    review_id       BIGINT NOT NULL,
    saved_file_path VARCHAR,
    create_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    edit_timestamp   TIMESTAMP,
    CONSTRAINT fk_review_p
        FOREIGN KEY (review_id)
            REFERENCES Reviews (review_id)
    ON UPDATE CASCADE
    ON DELETE NO ACTION
);