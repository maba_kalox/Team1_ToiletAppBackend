CREATE TABLE Reviews
(
    review_id        BIGSERIAL PRIMARY KEY,
    toilet_id        BIGINT                              NOT NULL,
    user_id          BIGINT                              NOT NULL,
    comment          VARCHAR,
    rating           SMALLINT,
    create_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    edit_timestamp   TIMESTAMP,
    CONSTRAINT fk_user_r
        FOREIGN KEY (user_id)
            REFERENCES Users (user_id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT fk_toilet_r
        FOREIGN KEY (toilet_id)
            REFERENCES Toilets (toilet_id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    UNIQUE (toilet_id, user_id)
);