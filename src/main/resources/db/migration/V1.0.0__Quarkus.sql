CREATE TABLE Users
(
    user_id   BIGSERIAL PRIMARY KEY,
    is_active BOOLEAN NOT NULL DEFAULT FALSE,
    google_id VARCHAR NOT NULL UNIQUE
);

CREATE TABLE Toilets
(
    toilet_id        BIGSERIAL PRIMARY KEY,
    user_id          BIGINT                              NOT NULL,
    create_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    edit_timestamp   TIMESTAMP,
    geo_point        geography(POINT),
    is_free          BOOLEAN                             NOT NULL,
    CONSTRAINT fk_user_t
        FOREIGN KEY (user_id)
            REFERENCES Users (user_id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);