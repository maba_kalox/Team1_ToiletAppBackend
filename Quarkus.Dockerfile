#
# This Dockerfile is used in order to build a container that runs the Quarkus application in JVM mode
#
FROM maven:3.6.0-jdk-11-slim AS build
WORKDIR /home/app
COPY src ./src
COPY pom.xml ./

RUN openssl genrsa -out rsaPrivateKey.pem 2048 \
    && openssl rsa -pubout -in rsaPrivateKey.pem -out publicKey.pem \
    && openssl pkcs8 -topk8 -nocrypt -inform pem -in rsaPrivateKey.pem -outform pem -out privateKey.pem \
    && mv ./publicKey.pem ./src/main/resources/keys/ \
    && mv ./privateKey.pem ./src/main/resources/keys/ \
    && rm ./rsaPrivateKey.pem

RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
FROM registry.access.redhat.com/ubi8/ubi-minimal:8.3

ARG JAVA_PACKAGE=java-11-openjdk-headless
ARG RUN_JAVA_VERSION=1.3.8
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en'
# Install java and the run-java script
# Also set up permissions for user `1001`
RUN microdnf install curl ca-certificates ${JAVA_PACKAGE} \
    && microdnf update \
    && microdnf clean all \
    && mkdir /deployments \
    && chown 1001 /deployments \
    && chmod "g+rwX" /deployments \
    && chown 1001:root /deployments \
    && curl https://repo1.maven.org/maven2/io/fabric8/run-java-sh/${RUN_JAVA_VERSION}/run-java-sh-${RUN_JAVA_VERSION}-sh.sh -o /deployments/run-java.sh \
    && chown 1001 /deployments/run-java.sh \
    && chmod 540 /deployments/run-java.sh \
    && echo "securerandom.source=file:/dev/urandom" >> /etc/alternatives/jre/conf/security/java.security

# Configure the JAVA_OPTIONS, you can add -XshowSettings:vm to also display the heap size.
ENV JAVA_OPTIONS="-Dquarkus.http.host=0.0.0.0 -Djava.util.logging.manager=org.jboss.logmanager.LogManager"
# We make four distinct layers so if there are application changes the library layers can be re-used
COPY --chown=1001 --from=build /home/app/target/quarkus-app/lib/ /deployments/lib/
COPY --chown=1001 --from=build /home/app/target/quarkus-app/*.jar /deployments/
COPY --chown=1001 --from=build /home/app/target/quarkus-app/app/ /deployments/app/
COPY --chown=1001 --from=build /home/app/target/quarkus-app/quarkus/ /deployments/quarkus/

RUN mkdir -p /public/photos \
    && chown -R 1001:root /public \
    && chmod -R 755 /public

RUN (cd /usr/local/sbin && curl -O https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh) \
    && chmod +x /usr/local/sbin/wait-for-it.sh

USER 1001

#ENTRYPOINT [ "/deployments/run-java.sh" ]
CMD [ "/deployments/run-java.sh" ]
